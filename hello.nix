let pkgs = import <nixpkgs> {};
in

pkgs.stdenv.mkDerivation (finalAttrs: {
  pname = "purescript";
  version = "2.12.1";
  src = ../Downloads/linux64.tar.gz;

  # src = builtins.fetchTarball {
    #   url = "/home/vamshi/Downloads/linux64.tar.gz";
    # url = "https://github.com/purescript/purescript/releases/tag/v0.15.6.tar.gz";
    # url = "mirror://gnu/hello/hello-${finalAttrs.version}.tar.gz";
    # sha256 = "sha256-jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=";
    # };
})
